import os
import yaml
import shutil
from yaml.loader import FullLoader

config = yaml.load(open('config.yaml'), Loader=FullLoader)
province = config['scenario_number']

### Copy the Hydro data for the given province into the Hydro_Data folder
original = f"silver/SILVER_Data/user_inputs/Hydro_Data-{province}/"
files = os.listdir(original)
destination = "silver/SILVER_Data/user_inputs/Hydro_Data/"
old_files = os.listdir(destination)
for file in old_files:
    os.remove(destination+file)
for input_file in files:
    shutil.copyfile(original+input_file, destination+input_file)


#### Copy VRE generation data for the given province into the VRE_Resource_Analysis folder
original = f"silver/SILVER_Data/user_inputs/VRE_Resource_Analysis-{province}/"
folders = os.listdir(original)

destination = "silver/SILVER_Data/user_inputs/VRE_Resource_Analysis/"
old_folders = os.listdir(destination)
print(old_folders)
for folder in old_folders:
    shutil.rmtree(destination+folder)
print(os.listdir(destination))
for folder in folders:
    shutil.copytree(original+folder, destination+folder)
print(os.listdir(destination))