import yaml
from yaml.loader import FullLoader

yml = yaml.load(open('../../../config.yaml'), Loader=FullLoader)
cfg = open('minpower/configuration/minpower.cfg', 'w+')
txt = open('minpower.txt', 'r')

for line in txt:
    if 'hours_commitment' in line:
        cfg.write(f"hours_commitment = {yml['hours_commitment']}\n")
        continue
    elif 'mipgap' in line:
        if '#' in line:
            continue
        cfg.write(f"mipgap = {yml['mipgap']}\n")
        continue
    else:
        cfg.write(line)